package com.altynaiberembaeva.sokobangameandroid;

import android.os.Handler;
import android.os.Looper;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

public class ConnectToServer implements Runnable {

        private Thread thread;
        private String level;
        private String responseMessage = "";
        private Model model;

        public ConnectToServer(String level, Model model) {
            thread = new Thread(this);
            this.level = level;
            this.model = model;
        }

        public void start() {
            thread.start();
            try {
                thread.join(3000);
            } catch (InterruptedException e) {
                final Handler h = new Handler(Looper.getMainLooper());
                h.post(() -> model.showTheErrorMessage());
                System.out.println("Lol");

            }
        }

        @Override
        public void run() {
            ObjectOutputStream output = null;
            ObjectInputStream input = null;
            Socket server = null;
            try {
                server = new Socket("194.152.37.7", 4446);
                output = new ObjectOutputStream(server.getOutputStream());
                input = new ObjectInputStream(server.getInputStream());
                output.writeUTF(level);
                output.flush();
                responseMessage = input.readUTF();
                model.didGetLevel(responseMessage);

            } catch (IOException e) {
                final Handler h = new Handler(Looper.getMainLooper());
                h.post(() -> model.showTheErrorMessage());
                e.printStackTrace();

            } finally {
                try {
                    if (server != null)
                        server.close();
                    if (output != null)
                        output.close();
                    if (input != null)
                        input.close();
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }
        }
    }