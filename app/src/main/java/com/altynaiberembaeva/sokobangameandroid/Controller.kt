package com.altynaiberembaeva.sokobangameandroid

import android.view.GestureDetector
import android.view.MenuItem
import android.view.MotionEvent
import android.view.View
import kotlin.math.abs

class Controller : View.OnTouchListener, GestureDetector.SimpleOnGestureListener, View.OnClickListener  {

    private var model: Model
    private val gesture: GestureDetector
    val SWIPE_THRESHOLD: Float = 100f
    val SWIPE_VELOCITY_THRESHOLD: Float = 100f

    constructor(viewer: ViewerActivity) {
        model = Model(viewer)
        gesture = GestureDetector(this)
    }

    fun getModel(): Model {
        return model

    }

    override fun onTouch(view: View, event: MotionEvent): Boolean {
        return gesture.onTouchEvent(event)

    }

    override fun onDown(e : MotionEvent): Boolean {
        return true

    }

    override fun onFling(eventOne: MotionEvent, eventTwo: MotionEvent, velocityX: Float, velocityY: Float): Boolean {
        var result = false

        val differenceY = eventTwo.y - eventOne.y
        val differenceX = eventTwo.x - eventOne.x

        if (abs(differenceX) > abs(differenceY)) {
            if (abs(differenceX) > SWIPE_THRESHOLD && abs(velocityX) > SWIPE_VELOCITY_THRESHOLD) {
                if (differenceX > 0)
                    moveToTheRight()
                else moveToTheLeft()

                result = true
            }

        } else if (abs(differenceY) > SWIPE_THRESHOLD && abs(velocityY) > SWIPE_VELOCITY_THRESHOLD) {
            if (differenceY > 0)
                moveToTheBottom()
            else moveToTheTop()

            result = true
        }
        return result
    }

    fun moveToTheRight() {
        model.move("Right")
    }

    fun moveToTheLeft() {
        model.move("Left")
    }

    fun moveToTheTop() {
        model.move("Up")
    }

    fun moveToTheBottom() {
        model.move("Down")

    }

    fun pullOutButtonsMenu(view: MenuItem) {
        when (view.itemId) {

            R.id.levelOne -> {
                model.levelOne()
            }
            R.id.levelTwo -> {
                model.levelTwo()
            }
            R.id.levelThree -> {
                model.levelThree()
            }
            R.id.levelFour -> {
                model.getLevelFromFile(1)
            }
            R.id.levelFive -> {
                model.getLevelFromFile(2)
            }
            R.id.levelSix -> {
                model.getLevelFromFile(3)
            }
            R.id.levelSeven ->{
                model.getServerLevel("levelSeven.txt")
            }
            R.id.levelEight ->{
                model.getServerLevel("levelEight.txt")
            }
            R.id.levelNine ->{
                model.getServerLevel("levelNine.txt")
            }
        }
    }
            override fun onClick(view: View) {
    }
}
