package com.altynaiberembaeva.sokobangameandroid

import android.content.res.AssetManager
import android.widget.Toast
import java.io.*

class Model {

    private val viewer: ViewerActivity
    private lateinit var map: Array<IntArray>
    private var indexX: Int
    private var indexY: Int
    private lateinit var currentServerLevelName: String

    constructor(viewer: ViewerActivity) {
        this.viewer = viewer
        indexY = 0
        indexX = 0
        levelOne()

    }

    fun levelOne() {
        indexY = 2
        indexX = 2
        map = arrayOf(
                intArrayOf(2, 2, 2, 2, 2, 2, 2, 2, 2, 2),
                intArrayOf(2, 0, 0, 0, 0, 0, 0, 0, 4, 2),
                intArrayOf(2, 0, 1, 0, 0, 0, 0, 0, 0, 2),
                intArrayOf(2, 2, 2, 2, 2, 0, 0, 0, 0, 2),
                intArrayOf(2, 0, 0, 0, 2, 0, 0, 0, 0, 2),
                intArrayOf(2, 0, 3, 0, 2, 0, 0, 0, 0, 2),
                intArrayOf(2, 0, 0, 0, 2, 0, 0, 0, 0, 2),
                intArrayOf(2, 0, 0, 0, 0, 0, 0, 0, 0, 2),
                intArrayOf(2, 0, 0, 0, 0, 0, 0, 0, 0, 2),
                intArrayOf(2, 2, 2, 2, 2, 2, 2, 2, 2, 2))

    }

    fun levelTwo() {
        indexY = 2
        indexX = 2
        map = arrayOf(
                intArrayOf(2, 2, 2, 2, 2, 2, 2, 2, 2, 2),
                intArrayOf(2, 0, 0, 0, 0, 0, 0, 0, 0, 2),
                intArrayOf(2, 0, 1, 0, 0, 0, 0, 3, 0, 2),
                intArrayOf(2, 0, 0, 0, 0, 2, 2, 2, 2, 2),
                intArrayOf(2, 0, 0, 0, 0, 0, 0, 0, 4, 2),
                intArrayOf(2, 0, 3, 0, 0, 0, 0, 0, 0, 2),
                intArrayOf(2, 2, 2, 2, 0, 0, 0, 0, 0, 2),
                intArrayOf(2, 0, 0, 0, 0, 0, 0, 0, 0, 2),
                intArrayOf(2, 4, 0, 0, 0, 0, 0, 0, 0, 2),
                intArrayOf(2, 2, 2, 2, 2, 2, 2, 2, 2, 2))

    }

    fun levelThree() {
        indexY = 2
        indexX = 2
        map = arrayOf(
                intArrayOf(2, 2, 2, 2, 2, 2, 2, 2, 2, 2),
                intArrayOf(2, 0, 0, 0, 0, 0, 0, 0, 0, 2),
                intArrayOf(2, 0, 1, 0, 0, 0, 0, 3, 0, 2),
                intArrayOf(2, 0, 0, 0, 0, 2, 2, 2, 2, 2),
                intArrayOf(2, 0, 0, 0, 0, 0, 0, 0, 0, 2),
                intArrayOf(2, 0, 0, 0, 0, 0, 3, 0, 0, 2),
                intArrayOf(2, 0, 0, 0, 2, 2, 2, 2, 2, 2),
                intArrayOf(2, 0, 0, 0, 0, 0, 0, 0, 0, 2),
                intArrayOf(2, 4, 0, 0, 0, 0, 0, 0, 4, 2),
                intArrayOf(2, 2, 2, 2, 2, 2, 2, 2, 2, 2))

    }

    fun move(direction: String?) {
        when (direction) {

            "Left" -> {
                moveLeft()
            }
            "Up" -> {
                moveUp()
            }
            "Right" -> {
                moveRight()
            }
            "Down" -> {
                moveDown()
            }
            else -> return
        }
        theEndOfTheLevel()
        viewer.update()
    }

    private fun moveLeft() {

        if (map[indexY][indexX - 1] != SokobanProperties.WALL && map[indexY][indexX - 1] != SokobanProperties.TARGETS) {

            if (map[indexY][indexX - 1] == SokobanProperties.BOX) {

                if (map[indexY][indexX - 2] != SokobanProperties.WALL) {

                    map[indexY][indexX - 2] = SokobanProperties.BOX

                } else {
                    return
                }
            }
            map[indexY][indexX] = SokobanProperties.FLOOR
            indexX -= 1

            map[indexY][indexX] = SokobanProperties.PLAYER
        }
    }

    private fun moveUp() {

        if (map[indexY - 1][indexX] != SokobanProperties.WALL && map[indexY - 1][indexX] != SokobanProperties.TARGETS) {

            if (map[indexY - 1][indexX] == SokobanProperties.BOX) {

                if (map[indexY - 2][indexX] != SokobanProperties.WALL) {

                    map[indexY - 2][indexX] = SokobanProperties.BOX

                } else {
                    return
                }
            }
            map[indexY][indexX] = SokobanProperties.FLOOR
            indexY -= 1

            map[indexY][indexX] = SokobanProperties.PLAYER
        }
    }

    private fun moveRight() {

        if (map[indexY][indexX + 1] != SokobanProperties.WALL && map[indexY][indexX + 1] != SokobanProperties.TARGETS) {

            if (map[indexY][indexX + 1] == SokobanProperties.BOX) {

                if (map[indexY][indexX + 2] != SokobanProperties.WALL) {

                    map[indexY][indexX + 2] = SokobanProperties.BOX

                } else {
                    return
                }
            }
            map[indexY][indexX] = SokobanProperties.FLOOR
            indexX += 1

            map[indexY][indexX] = SokobanProperties.PLAYER
        }
    }

    private fun moveDown() {

        if (map[indexY + 1][indexX] != SokobanProperties.WALL && map[indexY + 1][indexX] != SokobanProperties.TARGETS) {

            if (map[indexY + 1][indexX] == SokobanProperties.BOX) {

                if (map[indexY + 2][indexX] != SokobanProperties.WALL) {

                    map[indexY + 2][indexX] = SokobanProperties.BOX

                } else {
                    return
                }
            }
            map[indexY][indexX] = SokobanProperties.FLOOR
            indexY += 1

            map[indexY][indexX] = SokobanProperties.PLAYER
        }
    }

    fun getMap(): Array<IntArray> {
        return map

    }

    fun convertStringToMap(mapString: String): Array<IntArray> {

        var tempMapList: MutableList<MutableList<Int>> = mutableListOf<MutableList<Int>>()
        tempMapList.add(mutableListOf<Int>())
        var i = 0

        for (element in mapString) {

            if (element.isDigit()) {
                tempMapList[i].add(element.toString().toInt())

            }
            if (element == '\n') {
                i += 1
                tempMapList.add(mutableListOf<Int>())
            }
        }
        var tempMapArray = arrayOfNulls<IntArray>(tempMapList.size)
        i = 0

        tempMapList.forEach {

            tempMapArray[i] = tempMapList[i].toIntArray()
            i += 1
        }
        return tempMapArray as Array<IntArray>
    }

    fun сontentFromFile(path: String, viewer: ViewerActivity): String {
        var result = ""
        val assetsManager: AssetManager = viewer.getAssets()

        try {
            val inputReader = InputStreamReader(assetsManager.open(path))
            val bufferedReader = BufferedReader(inputReader)

            while (true) {
                val unicodeChar = bufferedReader.read()
                if (unicodeChar == -1)
                    break

                val inputChar = unicodeChar.toChar()
                if ((inputChar in '0'..'9') || (inputChar == '\n')) {
                    result += inputChar
                }
            }

        } catch (fne: FileNotFoundException) {
            Toast.makeText(viewer, "Уровень из файла не найден! $path", Toast.LENGTH_LONG).show()
        }

        result = result.trim()
        return result
    }

    fun getLevelFromFile(levelNumber: Int) {
        when (levelNumber) {

            1 -> {
                indexY = 2
                indexX = 7
                map = convertStringToMap(сontentFromFile("levels/levelFour.txt", viewer))
            }
            2 -> {
                indexY = 2
                indexX = 2
                map = convertStringToMap(сontentFromFile("levels/levelFive.txt", viewer))
            }
            3 -> {
                indexY = 8
                indexX = 5
                map = convertStringToMap(сontentFromFile("levels/levelSix.txt", viewer))
            }
        }
    }

    fun getServerLevel(levelName: String) {
        currentServerLevelName = levelName
        val connectToServer = ConnectToServer(levelName, this)
        connectToServer.start()

    }

    fun didGetLevel(level: String) {
        when (currentServerLevelName) {

            "levelSeven.txt" -> {
                indexY = 2
                indexX = 2
                map = convertStringToMap(level)
            }
            "levelEight.txt" -> {
                indexY = 2
                indexX = 2
                map = convertStringToMap(level)
            }
            "levelNine.txt" -> {
                indexY = 2
                indexX = 2
                map = convertStringToMap(level)
            }
        }
        viewer.update()
    }

    fun showTheErrorMessage() {
        viewer.showTheErrorMessage()
    }

    fun theEndOfTheLevel() {
        for (i in map) {
            for (j in i) {
                if (j == 4) {
                    return
                }
            }
        }
        viewer.winDialog()
    }
}

