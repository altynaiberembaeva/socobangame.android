package com.altynaiberembaeva.sokobangameandroid

import android.graphics.*
import android.view.View

class CanvasSokoban : View {
    private var model: Model
    private var paint: Paint
    private var playerIcon: Bitmap
    private var targetIcon: Bitmap
    private var donutIcon: Bitmap
    private val wallIcon: Bitmap

    constructor(view: ViewerActivity, model: Model) : super(view) {
        this.model = model
        paint = Paint()
        playerIcon = BitmapFactory.decodeResource(resources, R.drawable.girl)
        targetIcon = BitmapFactory.decodeResource(resources, R.drawable.cancel)
        donutIcon = BitmapFactory.decodeResource(resources, R.drawable.donut)
        wallIcon = BitmapFactory.decodeResource(resources, R.drawable.wall)
    }

    public override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)
        setBackgroundColor(Color.YELLOW)
        var x = 0
        var y = 0
        var width = 108
        var height = 160
        var map: Array<IntArray> = model.getMap()
        for (i in map) {
            for (j in i) {
                if (j == 1) {
                    canvas.drawBitmap(playerIcon,null, Rect(x, y, x + width,y + height), paint)
                } else if (j == 2) {
                    canvas.drawBitmap(wallIcon,null, Rect(x, y, x + width,y + height), paint)
                } else if (j == 3) {
                    canvas.drawBitmap(donutIcon,null, Rect(x, y, x + width,y + height), paint)
                } else if (j == 4) {
                    canvas.drawBitmap(targetIcon,null, Rect(x, y, x + width,y + height), paint)
                } else if (j == 0) {

                } else return

                x += width
            }
            x = 0
            y += height
        }
    }
    fun update() {
        invalidate()
    }
}

















