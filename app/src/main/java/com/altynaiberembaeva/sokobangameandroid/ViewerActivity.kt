package com.altynaiberembaeva.sokobangameandroid

import android.app.AlertDialog
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity

class ViewerActivity : AppCompatActivity {

    private val controller: Controller
    private lateinit var canvasSokoban: CanvasSokoban

    constructor(){
        controller = Controller(this)

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
         canvasSokoban = CanvasSokoban(this, controller.getModel())
         canvasSokoban.setOnTouchListener(controller)
         setContentView(canvasSokoban)

    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.menu, menu)
        return true
    }

        fun update(){
        canvasSokoban.update()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        controller.pullOutButtonsMenu(item)
        update()
        return true

    }

    fun showTheErrorMessage(){
        Toast.makeText(this, "Нет соединения с сервером, пожалуйста, запустите сервер!!!"
                , Toast.LENGTH_LONG).show()

    }

    fun winDialog(){
        AlertDialog.Builder(this, AlertDialog.THEME_HOLO_LIGHT)
                .setTitle("\uD83D\uDC9B\uD83D\uDC9B\uD83D\uDC9B\uD83D\uDC9B МОЛОДЕЕЕЦ!!! \uD83D\uDC9B\uD83D\uDC9B\uD83D\uDC9B\uD83D\uDC9B ")
                .setMessage("  \uD83D\uDC9B\uD83D\uDC9B ВЫ ПРОШЛИ УРОВЕНЬ \uD83D\uDC9B\uD83D\uDC9B")
                .setNegativeButton(android.R.string.no, null)
                .show()
    }
}